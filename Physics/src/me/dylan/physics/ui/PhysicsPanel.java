package me.dylan.physics.ui;

import javax.swing.JPanel;

public class PhysicsPanel extends JPanel {

	public PhysicsPanel() {
		int frames = 0;
		long lastTime = System.currentTimeMillis();
		while (true) {
			long currTime = System.currentTimeMillis();
			if (Math.floor((currTime - lastTime)) >= frames) {
				frames++;
			}
			if (currTime - lastTime >= 1000) {
				lastTime = currTime;
				System.out.println("FPS: " + frames);
				frames = 0;
			}
		}
	}
	
}
