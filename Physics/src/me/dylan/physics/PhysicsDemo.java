package me.dylan.physics;

import javax.swing.JFrame;

import me.dylan.physics.ui.PhysicsPanel;

public class PhysicsDemo {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Physics");
		
		frame.getContentPane().add(new PhysicsPanel());
		
		frame.setSize(1000, 800);
		frame.setLocationRelativeTo(null);
		
		frame.setVisible(true);
	}
	
}
