import java.util.Scanner;


public class SpeedChecker {																																										
	
	private static Scanner scanner = null;
	private static String name = "";
	
	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		System.out.println("Hello Officer, please enter your name.");
		name = scanner.nextLine();
		System.out.println("Welcome Officer " + name + ", we need you to catch the worst type of all criminals, the speeders.\nMuch worse than any murderer, these wreckless drivers cause a threat to our city, and your mission is to take them down.");
		System.out.println("If you accept your mission please type yes.");
		if (!scanner.nextLine().toLowerCase().contains("yes")) {
			System.out.println("Well that's unfortunate... " + name + " you're fired!");
			return;
		}
		System.out.print("Initiating " + name + "'s Police Gadget");
		for (int i = 0; i < 3; i ++) {
			System.out.print(".");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
		printDividers();
		System.out.println("Police Systems Online: Version 1.0");
		System.out.println("Officer: " + name);
		System.out.println("  Please select a task, type the corresponding key. ");
		System.out.println("    - (a) Track Speed");
		System.out.println("    - (b) Ask for a promotion");
		String selection = scanner.nextLine();
		if (selection.equalsIgnoreCase("a")) {
			printDividers();
			System.out.println("Speed Checker Tool: Version 1.4");
			System.out.println("  Officer " + name + " please enter in the speed limit of the road with the lines.");
			int speedLimit = scanner.nextInt();
			System.out.println("  Alright thank you. Now please enter the time it took the car to cross the 20 yard lines. (in seconds)");
			int seconds = scanner.nextInt();
			double mph = 20 / seconds * 3600 / 1760;
			System.out.print("  Calculating speed");
			for (int i = 0; i < 3; i ++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.print(".");
			}
			System.out.println();
			if (mph < speedLimit - 10) {
				System.out.println("  The driver is going very slow under the speed limit.");
				fail();
			} else if (mph <= speedLimit && mph >= speedLimit - 10) {
				System.out.println("  The driver is driving at the appropriate speed limit.");
				fail();
			} else if (mph > speedLimit && mph <= speedLimit + 5) {
				System.out.println("  The driver is going a little faster than the speed limit, but shouldn't be arrested.");
				fail();
			} else {
				System.out.println("  The driver is speeding at " + mph + " mph! Go get him!");
				printDividers();
				carChase();
			}
		} else if (selection.equalsIgnoreCase("b")){
			printDividers();
			System.out.println(name + ": \"Can I have a promotion?\"");
			wait(3);
			System.out.println("Captain: \"Are you kidding me?\"");
			wait(2);
			System.out.println("Captain: \"You haven't even caught a criminal yet.\"");
			wait(3);
			System.out.println(name + ": \"But Sir, I've been a diligent officer.\"");
			wait(3);
			System.out.println("Captain: \"Get out of here. You're fired officer " + name + "!\"");
		}
	}
	
	public static void fail() {
		System.out.println("  You are supposed to be finding speeding drivers. You are fired officer " + name + "!");
		printDividers();
	}
	
	public static void printDividers() {
		for (int i = 0; i < 50; i ++) {
			System.out.print("=");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
	}
	
	public static void carChase() {
		System.out.println("POLICE CHASE:");
		System.out.println("You started your car!");
		wait(2);
		System.out.println("You see the criminal driving off!");
		wait(2);
		System.out.println("The car is right in front of you!");
		System.out.println("He ran right through a redlight quick, make a decision!");
		printDividers();
		System.out.println("Should you (a) wait, or (b) go through the light?");
		scanner.nextLine();
		if (scanner.nextLine().equalsIgnoreCase("b")) {
			System.out.println("Good choice you're right behind him!");
			wait(2);
			System.out.println("A newspaper flew in your face and you didn't see which way he went.");
			printDividers();
			System.out.println("Turn (a) left or (b) right?");
			if (scanner.nextLine().equalsIgnoreCase("a")) {
				System.out.println("You found him! Go get him!");
				wait(3);
				System.out.println("You caught the criminal! Now get him off the streets.");
				wait(2);
				System.out.println("Great job Officer " + name + "! You caught the criminal. You are now promoted!");
			} else {
				System.out.println("I think you're right!");
				wait(2);
				System.out.println("\"No! I lost him.\"");
				System.out.println("You're fired officer " + name + "!");
			}
		} else {
			System.out.println("You decided to wait for the red light");
			wait(3);
			System.out.println("It's green! But it looks like the criminal got away!");
			System.out.println("You are fired officer " + name + " give me your gun and your badge.");
		}
	
	}
	
	public static void wait(int timeInSeconds) {
		try {
			Thread.sleep(timeInSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*private static Clip clip;
	
	public static void carChase() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(getClass().getResource("/Police Siren 3-SoundBible.com-553177907.wav"));
					clip.open(inputStream);
					clip.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}).start();
	}*/
	
}
