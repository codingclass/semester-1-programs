import javax.swing.JFrame;


public class Magic {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("MagicSquares");
		
		MagicSquare square = new MagicSquare(4, 4);
		frame.add(square);
		frame.pack();
		
		frame.setSize(540, 200);
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
	}
	
}
