import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class MagicSquare extends JPanel {
	
	private int width;
	private int height;
	
	private final int xIn = 30;
	private final int yIn = 20;
	
	private JTextField[][] fields;
	private JButton compute;
	private JLabel output;
	
	public MagicSquare(int width, int height) {
		this.setLayout(null);
		this.width = width;
		this.height = height;
		fields = new JTextField[width][height];
		init();
	}
	
	public void init() {
		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y ++) {
				JTextField field = new JTextField();
				field.setSize(50, 20);
				field.setLocation(60 * x + xIn, 30 * y + yIn);
				fields[x][y] = field;
				this.add(field);
			}
		}
		
		compute = new JButton("Compute MagicSquare");
		compute.setSize(200, 20);
		compute.setLocation(xIn + 60 * fields.length + 50, 30 * fields[0].length + yIn);
		
		compute.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				computeSquare();
			}
			
		});
		
		this.add(compute);
		
		output = new JLabel("No magic square checked yet ");
		output.setSize(300, 20);
		output.setLocation(xIn + 60 * fields.length + 50, 70);
		
		this.add(output);
		
		JButton clear = new JButton("Clear boxes");
		clear.setSize(270, 20);
		clear.setLocation(10, 30 * fields[0].length + yIn);
		
		clear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < fields.length; i ++) {
					for (int j = 0; j < fields[i].length; j ++) {
						fields[i][j].setText("");
					}
				}
			}
			
		});
		
		this.add(clear);
		
	}
	
	private void computeSquare() {
		int[] horizontal = new int[height];
		int[] vertical = new int[width];
		int[] diagnol = new int[2];
		for (int x = 0; x < fields.length; x ++) {
			for (int y = 0; y < fields[x].length; y ++) {
				try {
					horizontal[y] += Integer.parseInt(fields[x][y].getText());
					vertical[x] += Integer.parseInt(fields[x][y].getText());
					if (x == y)
						diagnol[0] += Integer.parseInt(fields[x][y].getText());
					if (x + y == width - 1)
						diagnol[1] += Integer.parseInt(fields[x][y].getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "You entered invalid values for some boxes!");
					return;
				}
			}
		}
		
		int number = horizontal[0];
		
		for (int i = 0; i < height; i ++) {
			if (horizontal[i] != number) {
				output.setText("Not a magic square");
				return;
			}
		}
		
		for (int i = 0; i < width; i ++) {
			if (vertical[i] != number) {
				output.setText("Not a magic square");
				return;
			}
		}
		
		for (int i = 0; i < 2; i ++) {
			if (diagnol[i] != number) {
				output.setText("Not a magic square");
				return;
			}
		}
		
		output.setText("That is a magic square with the number " + number + "!");
		
	}
	
}
