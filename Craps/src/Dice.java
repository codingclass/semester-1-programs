import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;


public class Dice {
	
	private JWindow window;
	private Random random;
	
	public Dice() {
		window = new JWindow();
        window.setBackground(new Color(0,0,0,0));
        window.setAlwaysOnTop(true);

        JLabel dice1 = null, dice2 = null;
		try {
			File file = new File("src/dice.gif");
			dice1 = new JLabel(new ImageIcon(new URL("file:" + file.getAbsolutePath())));
			dice2 = new JLabel(new ImageIcon(new URL("file:" + file.getAbsolutePath())));
		} catch (IOException e) {
			e.printStackTrace();
		}
        window.add(dice1);
        window.add(dice2, BorderLayout.WEST);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(false);
        random = new Random();
	}
	
	public void toggle() {
		window.setVisible(!window.isVisible());
	}
	
	public int roll() {
		window.setVisible(true);
		int roll = random.nextInt(11) + 2;
		try {
			Thread.sleep(1000L + random.nextInt(1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		window.setVisible(false);
		return roll;
	}
	
}
