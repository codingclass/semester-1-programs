
public class Craps {
	
	private double bank;
	private int point = 0;
	private Dice dice;
	private double bet = 0.0D;
	
	public Craps(double bank) {
		this.bank = bank;
		this.dice = new Dice();
	}
	
	public void reset() {
		this.point = 0;
		this.bet = 0.0D;
	}
	
	public int getPoint() {
		return point;
	}
	
	public double getBet() {
		return bet;
	}
	
	public void setBalance(double balance) {
		this.bank = balance;
	}
	
	public double getBalance() {
		return this.bank;
	}
	
	public int rollDice(double bet) {
		int roll = dice.roll();
		this.bet += bet;
		System.out.println("You rolled a " + roll + "!");
		if (point == 0) {
			if (roll == 7 || roll == 11) {
				//win
				return 1;
			} else if (roll == 2 || roll == 3 || roll == 12) {
				//lose
				return -1;
			} else {
				point = roll;
				return 0;
			}
		} else {
			if (roll == 7) {
				return -1; //lose
			} else if (roll == point) {
				return 1; //winner
			} else {
				return 0;
			}
		}
	}
	
}
