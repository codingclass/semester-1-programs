import java.util.InputMismatchException;
import java.util.Scanner;



public class Game {
	
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to the game of Craps!\nPlease enter your initial bankroll: ");
		double initialBank = 0;
		while (true) {
			try {
				initialBank = scanner.nextDouble();
				if (initialBank <= 0) {
					System.out.println("Wow. What a cheater.");
					continue;
				}
				break;
			} catch (InputMismatchException e) {
				System.out.println("Invalid bank balance! Try again.");
				scanner.nextLine();
				continue;
			}
		}
		Craps craps = new Craps(initialBank);
		while (true) {
			craps.reset();
			System.out.println("Now, enter a bet for your first roll:");
			double bet = 0;
			while (true) {
				try {
					bet = scanner.nextDouble();
					if (bet < 0) {
						System.out.println("Wow. What a cheater.");
						continue;
					}
				} catch (InputMismatchException e) {
					System.out.println("You didn't enter a bet correctly.");
					scanner.nextLine();
					continue;
				}
				if (bet + craps.getBet() > craps.getBalance())
					System.out.println("You do not have that much money in your bank.");
				else
					break;
			}
			int roll = craps.rollDice(bet); 
			while (roll == 0) {
				System.out.println("You need to roll again, remember you need a " + craps.getPoint() + " to win! Your current bet is $" + craps.getBet() + "!");
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Please enter your bet:");
				while (true) {
					try {
						bet = scanner.nextDouble();
						if (bet < 0) {
							System.out.println("Wow. What a cheater.");
							continue;
						}
					} catch (InputMismatchException e) {
						System.out.println("You didn't enter a bet correctly.");
						scanner.nextLine();
						continue;
					}
					if (bet + craps.getBet() > craps.getBalance())
						System.out.println("You do not have that much money in your bank.");
					else
						break;
				}
				roll = craps.rollDice(bet);
			}
			if (roll == 1) {
				System.out.println("You win your bet of $" + craps.getBet() + "! You have been awarded $" + craps.getBet() * 2 + " back!");
				craps.setBalance(craps.getBalance() + craps.getBet());
			} else if (roll == -1) {
				System.out.println("You are terrible at this game. You lost your bet of $" + craps.getBet() + "!");
				craps.setBalance(craps.getBalance() - craps.getBet());
			}
			System.out.println("The money in your bank is $" + craps.getBalance() + "!");
			System.out.println("Would you like to play again? Type \"yes\" to play again, anything else to leave.");
			scanner.nextLine();
			if (!scanner.nextLine().equalsIgnoreCase("yes"))
				break;
		}
		System.out.println("Thank you for playing, you left with $" + craps.getBalance() + "!");
		System.out.println("Your profit was $" + (craps.getBalance() - initialBank) + "!");
		scanner.close();
		System.exit(0);
	}
	
}
