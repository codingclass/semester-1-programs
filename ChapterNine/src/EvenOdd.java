import java.util.InputMismatchException;
import java.util.Scanner;


public class EvenOdd {
	
	public static void main(String args[]) {
		System.out.println("Please input 10 integers.");
		Scanner scanner = new Scanner(System.in);
		int[] input = new int[10];
		for (int i = 0; i < input.length; i ++) {
			int number = 0;
			while (true) {
				try {
					number = scanner.nextInt();
					break;
				} catch (InputMismatchException e) {
					scanner.nextLine();
					System.out.println("That is not a valid integer!");
				}
			}
			input[i] = number;
			if (i != input.length - 1)
				System.out.println("Thank you now please enter another!");
		}
		System.out.println("Thank you for all the numbers.");
		int negativeSize = 0;
		int evenSize = 0;
		int oddSize = 0;
		//calculate sizes of arrays
		for (int i = 0; i < input.length; i ++) {
			int number = input[i];
			if (number < 0)
				negativeSize++;
			if (number % 2 == 0)
				evenSize++;
			else
				oddSize++;
		}
		//create arrays for each possibility
		int[] evenList = new int[evenSize];
		int[] oddList = new int[oddSize];
		int[] negativeList = new int[negativeSize];
		int evenIndex = 0;
		int oddIndex = 0;
		int negativeIndex = 0;
		for (int number : input) {
			if (number < 0)
				negativeList[negativeIndex++] = number;
			if (number % 2 == 0)
				evenList[evenIndex++] = number;
			else
				oddList[oddIndex++] = number;
		}
		System.out.println("Even numbers: " + cleanArray(evenList));
		System.out.println("Odd numbers: " + cleanArray(oddList));
		System.out.println("Negative numbers: " + cleanArray(negativeList));
		scanner.close();
	}
	
	public static String cleanArray(int[] array) { //substitution for Arrays.toString(string);
		String arrayString = "";
		for (int i = 0; i < array.length; i ++)
			arrayString += array[i] + ", ";
		return arrayString.substring(0, arrayString.length() > 2 ? arrayString.length() - 2 : arrayString.length());
	}
	
}
