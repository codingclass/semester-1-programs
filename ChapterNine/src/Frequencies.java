import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;


public class Frequencies {
	
	public static void main(String[] args) {
		double[] inputs = new double[10];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter 10 numbers.");
		for (int i = 0; i < inputs.length; i ++) {
			double input = 0;
			while (true) {
				try {
					input = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					scanner.nextLine();
					System.out.println("That is not a valid input!");
				}
			}
			inputs[i] = input;
			if (i != inputs.length - 1)
				System.out.println("Thank you, now please enter another number.");
		}
		double[] modes = getMode(inputs);
		Set<Double> modeSet = new HashSet<Double>();
	
		for(int i = 0; i < modes.length; i++){
		  modeSet.add(modes[i]);
		}
			
		System.out.println("Modes: ");
		for (double mode : modeSet) {
			if (mode != 0)
				System.out.println(" -> " + mode);
		}
		System.out.println("The median is " + getMedian(inputs) + "!");
		System.out.println("\nFrequency Table: (number locations respective to frequencies)");
		double[][] table = getFrequencies(inputs);
		System.out.println("Numbers: " + Arrays.toString(table[0]));
		System.out.println("Frequencies: " + Arrays.toString(table[1]));
		
		
		scanner.close();
	}
	
	private static double[] getMode(double[] array) { //the array is parallel to frequencies
		int[] frequencies = new int[array.length];
		for (int j = 0; j < array.length; j ++) {
			double number = array[j];
			int occurances = 0;
			for (int k = 0; k < array.length; k ++) {
				if (array[k] == number)
					occurances++;
			}
			frequencies[j] = occurances;
		}
		//sort out frequencies
		double modes[] = new double[array.length];
		int lastFrequency = 0;
		for (int i = 0; i < array.length; i ++) {
			if (frequencies[i] == lastFrequency)
				modes[i] = array[i];
			if (frequencies[i] > lastFrequency) {
				modes = new double[array.length];
				modes[i] = array[i];
				lastFrequency = frequencies[i];
			}
		}
		return modes;
	}
	
	public static double getMedian(double[] array) {
		Arrays.sort(array);
		return array.length % 2 == 0 ? (array[array.length / 2 - 1] + array[array.length / 2]) / 2 : Math.ceil(array[array.length / 2]);
	}
	
	public static double[][] getFrequencies(double[] array) {
		Arrays.sort(array);
		double[] frequencies = new double[array.length];
		for (int j = 0; j < array.length; j ++) {
			double number = array[j];
			int occurances = 0;
			for (int k = 0; k < array.length; k ++) {
				if (array[k] == number)
					occurances++;
			}
			frequencies[j] = occurances;
		}
		return new double[][] {array, frequencies};
	}
	
}
