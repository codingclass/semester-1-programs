import java.util.InputMismatchException;
import java.util.Scanner;


public class Averager {

	public static void main(String[] args) {
		double[] inputs = new double[10];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter 10 numbers.");
		for (int i = 0; i < inputs.length; i ++) {
			double input = 0;
			while (true) {
				try {
					input = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					scanner.nextLine();
					System.out.println("That is not a valid input!");
				}
			}
			inputs[i] = input;
			if (i != inputs.length - 1)
				System.out.println("Thank you, now please enter another number.");
		}
		double average = getAverage(inputs);
		System.out.println(average);
		int aboveLength = 0;
		for (int i = 0; i < inputs.length; i ++) {
			if (inputs[i] > average)
				aboveLength++;
		}
		double[] aboveAverage = new double[aboveLength];
		int aboveIndex = 0;
		for (int i = 0; i < inputs.length; i ++) {
			if (inputs[i] > average)
				aboveAverage[aboveIndex++] = inputs[i];
		}
		System.out.println("Numbers above average: " + cleanArray(aboveAverage));
		scanner.close();
	}
	
	public static String cleanArray(double[] array) { //substitution for Arrays.toString(string);
		String arrayString = "";
		for (int i = 0; i < array.length; i ++)
			arrayString += array[i] + ", ";
		return arrayString.substring(0, arrayString.length() > 2 ? arrayString.length() - 2 : arrayString.length());
	}
	
	private static double getAverage(double[] array) {
		double sum = 0;
		for (double element : array)
			sum += element;
		return (sum / array.length);
	}
	
}
