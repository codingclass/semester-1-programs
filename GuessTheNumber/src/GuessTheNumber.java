import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class GuessTheNumber {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		while (true) {
			int number = random.nextInt(100) + 1;
			System.out.println("Please guess a number 1-100! If you give up, just type -1.");
			int choice = 0, guesses = 1;
			while (true) {
				try {
					choice = scanner.nextInt();
					if (!(choice >= -1 && choice <= 100 && choice != 0)) {
						System.out.println("You're crazy! That's not a number from 1-100!");
						continue;
					}
					if (choice != -1) {
						if (choice == number)
							break;
						System.out.println("Try again but guess " + (number > choice ? "higher!" : "lower!"));
						guesses ++;
						continue;
					}
					break;
				} catch (InputMismatchException e) {
					System.out.println("The number must be a whole number. Try again.");
					scanner.nextLine();
					continue;
				}
			}
			if (choice == -1)
				System.out.println("You suck. The number was: " + number + "!");
			else
				System.out.println("Congratoulations you got the number in " + guesses + " guesses!");
			System.out.println("If you would like to play again just type \"yes\". If not, type anything else.");
			if (!scanner.next().equalsIgnoreCase("yes"))
				break;
		}
		System.out.println("Thank you for playing!");
		scanner.close();
	}
	
}
