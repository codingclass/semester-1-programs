import java.util.Scanner;


public class Main {
	
	private static Scanner scanner;
	private static Fraction fraction;
	
	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		System.out.println("Please enter in a fraction!");
		fraction = requestFraction();
		while (true) {
			promptInput();
			int choice = scanner.nextInt();
			if (choice == 0) {
				System.out.println("Enter a fraction to add");
				scanner.nextLine();
				Fraction addFraction = requestFraction();
				Fraction newFraction = fraction.addFraction(addFraction);
				System.out.println("Your answer is " + newFraction.toString() + "!");
			} else if (choice == 1) {
				System.out.println("Enter a fraction to subtract");
				scanner.nextLine();
				Fraction subtractFraction = requestFraction();
				Fraction newFraction = fraction.subtractFraction(subtractFraction);
				System.out.println("Your answer is " + newFraction.toString() + "!");
			} else if (choice == 2) {
				System.out.println("Enter a fraction to multiply");
				scanner.nextLine();
				Fraction multiplyFraction = requestFraction();
				Fraction newFraction = fraction.multiplyFraction(multiplyFraction);
				System.out.println("Your answer is " + newFraction.toString() + "!");
			} else if (choice == 3) {
				System.out.println("Enter a fraction to divide");
				scanner.nextLine();
				Fraction divideFraction = requestFraction();
				Fraction newFraction = fraction.divideFraction(divideFraction);
				System.out.println("Your answer is " + newFraction.toString() + "!");
			} else if (choice == 4) {
				System.out.println("Enter a new original fraction!");
				scanner.nextLine();
				fraction = requestFraction();
			} else if (choice == 5) {
				System.out.println("Thank you for using the fraction tester!");
				System.exit(0);
			} else {
				System.out.println("Invalid choice!");
			}
		}
	}
	
	public static void promptInput() {
		System.out.println("Your original fraction is: " + fraction.toString());
		System.out.println("Would you like to:");
		System.out.println("  - (0) Add the fraction?");
		System.out.println("  - (1) Subtract the fraction?");
		System.out.println("  - (2) Multiply the fraction?");
		System.out.println("  - (3) Divide the fraction?");
		System.out.println("  - (4) Change original fraction?");
		System.out.println("  - (5) Quit");
	}
	
	public static Fraction requestFraction() {
		String fractionString = "";
		while (!fractionString.contains("/")) {
			fractionString = scanner.nextLine();
			if (!fractionString.contains("/"))
				System.out.println("Your fraction doesn't contain a vinculum, try again!");
		}
		String[] fractionParts = fractionString.split("/");
		if (Integer.parseInt(fractionParts[1]) == 0) {
			System.out.println("Your fraction cannot be undefined!");
			System.exit(0);
		}
		Fraction fraction = new Fraction(Integer.parseInt(fractionParts[0]), Integer.parseInt(fractionParts[1]));
		return fraction;
	}
	
}
