
public class Fraction {
	
	private int numerator;
	private int denominator;
	
	public Fraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}
	
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	
	public int getNumerator() {
		return numerator;
	}
	
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	
	public int getDenominator() {
		return denominator;
	}
	
	public Fraction addFraction(Fraction fraction) {
		if (this.toString().equals("0")) {
			return fraction;
		} else if (fraction.toString().equals("0")) {
			return this;
		} else {
			int nn = (this.getNumerator() * fraction.getDenominator()) + (fraction.getNumerator() * this.getDenominator());
			int nd = this.getDenominator() * fraction.getDenominator();
			return new Fraction(nn, nd);
		}
	}
	
	public Fraction subtractFraction(Fraction fraction) {
		if (this.toString().equals("0")) {
			return fraction;
		} else if (fraction.toString().equals("0")) {
			return this;
		} else {
			int nn = (this.getNumerator() * fraction.getDenominator()) - (fraction.getNumerator() * this.getDenominator());
			int nd = this.getDenominator() * fraction.getDenominator();
			return new Fraction(nn, nd);
		}
	}
	
	public Fraction multiplyFraction(Fraction fraction) {
		int nn = this.getNumerator() * fraction.getNumerator();
		int nd = this.getDenominator() * fraction.getDenominator();
		return new Fraction(nn, nd);
	}
	
	public Fraction divideFraction(Fraction fraction) {
		int nn = this.getNumerator() * fraction.getDenominator();
		int nd = this.getDenominator() * fraction.getNumerator();
		return new Fraction(nn, nd);
	}
	
	@Override
	public String toString() {
		if (numerator == 0) {
			return "0";
		}
		if (denominator == 0) {
			return "undefined";
		}
		return numerator + "/" + denominator;
	}
	
}
