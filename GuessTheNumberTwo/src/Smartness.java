
public enum Smartness {
	
	STUPID(5, .2), AVERAGE(15, .4), SMART(25, .6), PRODIGY(40, .75), UNLUCKY_HACKER(70, .1), HACKER(70, .9);
	
	private int guessRange; //midpoint of range of 10 to guess until narrrowed down
	private double luck; //luckyness of the guess to be correct
	
	Smartness(int guessRange, double luck) {
		this.guessRange = guessRange;
		this.luck = luck;
	}
	
	public int getGuessRange() {
		return guessRange;
	}
	
	public double getLuck() {
		return luck;
	}
	
}
