import java.util.ArrayList;
import java.util.Random;


public class Computer {
	
	private Smartness smartness;
	private int learnedMin = 1;
	private int learnedMax = 100;
	private boolean needHigher;
	private Random random;
	private ArrayList<Integer> guesses = new ArrayList<Integer>();
	
	public Computer(Smartness smartness) {
		this.smartness = smartness;
		random = new Random();
	}
	
	public int calculateGuess(int number) { 
		System.out.println("I'm thinking...");
		try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int guess = 0;
		if (guesses.isEmpty()) {
			double luck = smartness.getLuck();
			int luckValue = (int) (luck * 100);
			int chance = random.nextInt(100) + 1;
			while (true) {
				if (chance < luckValue)
					guess = random.nextInt(10) + (number - 5);
				else if (chance > luckValue && chance < luckValue + 5)
					guess = random.nextInt(30) + (number - 5);
				else 
					guess = random.nextInt(100) + 1;
				if (guess >= learnedMin && guess <= learnedMax)
					break;
			}
			//TODO MAKE SURE THAT THE GUESS ISNT OVER 100 FOR A LUCKY GUESS!
		} else {
			int guessRangeMin = smartness.getGuessRange() - random.nextInt(5) + 1; //20
			//int guessRangeMax = smartness.getGuessRange() + random.nextInt(5) + 1; //30
			int guessRange = 0;
			if (needHigher) {
				while (true) {
					guessRange = random.nextInt(10) + guessRangeMin;
					if (guessRange + learnedMin >= learnedMax)
						guessRange = learnedMax - learnedMin;
					guess = learnedMin + random.nextInt(guessRange) + 1;
					
					if (guess <= learnedMax && !guesses.contains(guess))
						break;
				}
			} else {
				while (true) {
					guessRange = random.nextInt(10) + guessRangeMin;
					if (learnedMax - guessRange <= learnedMin)
						guessRange = learnedMax - learnedMin;
					guess = learnedMax - (random.nextInt(guessRange) + 1);
					
					if (guess >= learnedMin && !guesses.contains(guess))
						break;
				}
			}
		}
		if (guess < number) {
			learnedMin = guess;
			needHigher = true;
		} else if (guess > number) {
			learnedMax = guess;
			needHigher = false;
		}
		guesses.add(guess);
		return guess;
	}
	
	public Smartness getIntelligence() {
		return smartness;
	}
	
	public int getGuesses() {
		return guesses.size();
	}
	
}
