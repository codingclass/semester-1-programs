import java.util.InputMismatchException;
import java.util.Scanner;


public class GuessTheNumberTwo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Please enter a difficulty level for the computer: ");
			System.out.println("  - (1) Stupid");
			System.out.println("  - (2) Average");
			System.out.println("  - (3) Smart");
			System.out.println("  - (4) Prodigy");
			System.out.println("  - (5) Unlucky Hacker");
			System.out.println("  - (6) Hacker");
			Computer computer = null;
			while (true) {
				try {
					computer = new Computer(Smartness.values()[scanner.nextInt() - 1]);
					break;
				} catch (ArrayIndexOutOfBoundsException | InputMismatchException e) {
					System.out.println("Invalid difficulty level. Try again.");
					scanner.nextLine();
					continue;
				}
			}
			System.out.println("Your computer is " + computer.getIntelligence().toString().replace("_", " ") + "!");
			System.out.println("Please enter in a number 1-100!");
			int number = 0;
			while (true) {
				try {
					number = scanner.nextInt();
					if (!(number >= 1 && number <= 100)) {
						System.out.println("The number is not 1-100. Try again.");
						continue;
					}
					break;
				} catch (InputMismatchException e) {
					System.out.println("The number must be a whole number. Try again.");
					scanner.nextLine();
					continue;
				}
			}
			int guess = 0;
			while ((guess = computer.calculateGuess(number)) != number)
				System.out.println("The computer guessed " + guess + "!");
			System.out.println("The computer guessed " + guess + " and won in " + computer.getGuesses() + " guesses!");
			System.out.println("Would you like to re-run? Type \"yes\" to run it again, anything else to stop.");
			if (!scanner.next().equalsIgnoreCase("yes"))
				break;
		}
		System.out.println("Goodbye!");
		scanner.close();
	}
	
}
