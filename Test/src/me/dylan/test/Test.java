package me.dylan.test;

import java.awt.Color;

import javax.swing.JFrame;

public class Test {
	
	public static void makeWindow() {
		JFrame frame = new JFrame("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setBackground(Color.RED);
		frame.setBounds(50, 50, 1000, 800);
		frame.setVisible(true);
	}
	
}
