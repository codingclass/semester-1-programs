import java.util.Scanner;


public class BaseToExponent {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Enter a base for a power.");
			double base = scanner.nextDouble();
			System.out.println("Thanks, now enter an exponent for a power");
			double power = scanner.nextDouble();
			System.out.println(solveExponent(base, power));
			System.out.println("Would you like to rerun the code? Type yes to rerun, anything else to stop.");
			scanner.nextLine();
			if (!scanner.nextLine().toLowerCase().contains("yes")) break;
		}
		System.out.println("Thank you for using the program!");
		scanner.close();
	}
	
	public static String solveExponent(double base, double power) {
		return base + " to the " + power + " power is " + Math.pow(base, power) + "!";
	}
	
}
