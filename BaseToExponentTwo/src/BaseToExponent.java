import javax.swing.JFrame;


public class BaseToExponent {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Base to Exponent");
		frame.setSize(500, 400);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
		B2EPanel panel = new B2EPanel(frame);
		panel.setSize(frame.getSize());
		panel.setLayout(null);
		frame.getContentPane().add(panel);
	}
	
	public static String solveExponent(double base, double power) {
		return base + " to the " + power + " power is " + Math.pow(base, power) + "!";
	}
	
}
