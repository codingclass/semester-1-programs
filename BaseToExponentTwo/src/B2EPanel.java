import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.rmi.server.ExportException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.RestoreAction;


public class B2EPanel extends JPanel {
	
	private File file;
	private JTextArea textArea;
	private JButton export;
	
	public B2EPanel(JFrame frame) {
		this.setSize(frame.getSize());
		this.setBackground(Color.gray);
		file = null;
		
		initComponents();
		frame.pack();
		frame.setSize(this.getSize());
		//repaint();
	}
	
	public void initComponents() {
		textArea = new JTextArea("No file selected");
		textArea.setEditable(false);
		JScrollPane scroller = new JScrollPane(textArea);
		scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroller.setSize(this.getWidth() - 20, this.getHeight() - 105);
		scroller.setLocation(6, 25);
		
		JButton openFile = new JButton("Open File");
		openFile.setSize(this.getWidth(), 50);
		openFile.setLocation(0, (int) this.getHeight() - 75);
		openFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				int choice = fileChooser.showOpenDialog(B2EPanel.this);

				if (choice != JFileChooser.APPROVE_OPTION) {
					System.out.println("You didn't select a file!");
					return;
				}

				file = fileChooser.getSelectedFile();
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(file));
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				repaint();
				textArea.setText("");
				String line = null;
				try {
					while((line = reader.readLine()) != null ) {
						String[] parts = line.split("\\s+");
						double answer = Math.pow(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]));
						textArea.append(parts[0] + " ^ " + parts[1] + " = " + answer + "\n");
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				export.setVisible(true);
				try {
					reader.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
		});
		
		export = new JButton("Export ->");
		export.setSize(100, 20);
		export.setLocation(this.getWidth() - export.getWidth() - 10, 3);
		export.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showSaveDialog(B2EPanel.this) == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					if (!file.exists()) {
						try {
							file.createNewFile();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
					try {
						PrintWriter printWriter = new PrintWriter(new FileOutputStream(file));
						for (String line : textArea.getText().split("\\n"))
							printWriter.println(line);
						printWriter.close();
						System.out.println("Export Successful!");
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});
		
		this.add(scroller);
		this.add(openFile);
		this.add(export);
		
		openFile.setVisible(true);
		scroller.setVisible(true);
		export.setVisible(false);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.drawString(file == null ? "File: N/A" : "File: " + file.getName(), 10, 20);
	}
	
}
