import java.util.InputMismatchException;
import java.util.Scanner;


public class NumberCommas {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Enter a number to give it commas, enter -1 to quit!");
			int number = 0;
			try {
				number = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Not a valid integer!");
				scanner.nextLine();
			}
			if (number != -1) {
				System.out.println("Number with commas: " + addCommas(String.valueOf(number)));
			} else {
				break;
			}
		}
		scanner.close();
		System.out.println("Thank you for using the Number Comma Creator!");
	}
	
	private static String addCommas(String num) {
		return num.length() <= 3 ? num : addCommas(num.substring(0, num.length() - 3)) + "," + num.substring(num.length() - 3);
	}
	
}
