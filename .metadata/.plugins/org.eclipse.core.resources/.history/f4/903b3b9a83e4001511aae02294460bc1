import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class SentenceGenerator {
	
	private static List<String> nouns = new ArrayList<String>();
	private static List<String> verbs = new ArrayList<String>();
	private static List<String> history = new ArrayList<String>();
	private static List<String> usedNouns = new ArrayList<String>();
	private static List<String> usedVerbs = new ArrayList<String>();
	
	public static void main(String[] args) {
		System.out.println("Welcome to the Sentence Generator!");
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Please select a task!");
			System.out.println(" - (1) Add noun");
			System.out.println(" - (2) Add verb");
			System.out.println(" - (3) View nouns and verbs");
			System.out.println(" - (4) Clear nouns and verbs");
			System.out.println(" - (5) Generate Sentence");
			System.out.println(" - (6) Sentence History");
			System.out.println(" - (7) Reject Last Sentence");
			System.out.println(" - (8) Exit program");
			System.out.println("======================");
			int choice = 0;
			while (true) {
				try {
					choice = scanner.nextInt();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid choice! Pick again!");
					scanner.nextLine();
				}
			}
			scanner.nextLine();
			if (choice == 1) {
				System.out.println("Please enter a noun: ");
				String noun = "";
				while (true) {
					try {
						noun = scanner.nextLine();
						break;
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid noun! Pick again!");
						scanner.nextLine();
					}
				}
				nouns.add(noun);
				System.out.println("Added " + noun + " to nouns!");
			} else if (choice == 2) {
				System.out.println("Please enter a verb: ");
				String verb = "";
				while (true) {
					try {
						verb = scanner.nextLine();
						break;
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid verb! Pick again!");
						scanner.nextLine();
					}
				}
				verbs.add(verb);
				System.out.println("Added " + verb + " to verbs!");
			} else if (choice == 3) {
				System.out.println("==Nouns==");
				nouns.forEach(System.out::println);
				System.out.println("==Verbs==");
				verbs.forEach(System.out::println);
				delay();
			} else if (choice == 4) {
				System.out.println("Cleared nouns and verbs!");
				nouns.clear();
				verbs.clear();
			} else if (choice == 5) {
				if (nouns.size() < 3 || verbs.size() <= 0) {
					System.out.println("You need to have included at least three nouns and one verb!");
					delay();
					continue;
				}
				usedNouns.clear();
				usedVerbs.clear();
				int attempts = 0;
				while (true) {
					String nounOne = getRandomWord(WordType.NOUN);
					String verb = getRandomWord(WordType.VERB);
					String nounTwo = getRandomWord(WordType.NOUN);
					String nounThree = getRandomWord(WordType.NOUN);
					if (nounOne.equals("") || verb.equals("") || nounTwo.equals("")
							|| nounThree.equals("")) {
						System.out.println("You will need more words!");
						break;
					}
					String sentence = "The " + getRandomWord(WordType.NOUN) + " "
							+ getRandomWord(WordType.VERB) + " the "
							+ getRandomWord(WordType.NOUN) + " with a "
							+ getRandomWord(WordType.NOUN) + ".";
					if (!history.contains(sentence)) {
						System.out.println(sentence);
						history.add(sentence);
						break;
					} else {
						attempts ++;
						if (attempts >= 5) {
							System.out.println("You need more nouns/verbs!");
							break;
						}
					}
				}
			} else if (choice == 6) {
				System.out.println("==History==");
				history.forEach(System.out::println);
			} else if (choice == 7) {
				if (history.size() > 0) {
					System.out.println("Rejected last sentence!");
					history.remove(history.size() - 1);
				} else 
					System.out.println("You do not have any sentences to reject!");
			} else if (choice == 8) {
				break;
			}
			delay();
		}
		System.out.println("Thank you for using the sentence generator!");
		scanner.close();
	}
	
	public static String getRandomWord(WordType type) {
		if (type == WordType.NOUN) {
			String noun = "";
			while (true) {
				noun = nouns.get(ThreadLocalRandom.current().nextInt(nouns.size())); //generate noun
				if (!usedNouns.contains(noun)) { //never used noun
					System.out.println(noun);
					usedNouns.add(noun); //add to used noun
					return noun; //return the noun
				} else { //have used noun already
					continue; //try for new noun
				}
			}
		} else if (type == WordType.VERB) {
			String verb =  verbs.get(ThreadLocalRandom.current().nextInt(verbs.size()));
			if (!usedVerbs.contains(verb))
				usedVerbs.add(verb);
			else
				return "";
			return verb;
		}
		return "";
	}
	
	public enum WordType {
		NOUN, VERB;
	}
	
	private static void delay() {
		try {
			Thread.sleep(500L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
