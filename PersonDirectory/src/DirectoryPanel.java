import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;


public class DirectoryPanel extends JPanel {
	
	private PersonFile file;
	
	//labels
	
	private JLabel page;
	private JLabel person;
	private JLabel age;
	private JLabel socialSecurity;
	private int currentPage;
	private int pages;
	
	public DirectoryPanel() {
		this.setLayout(new BorderLayout());
		init();
		loadPeople();
	}
	
	private void init() {
		
		//containers
		
		JPanel north = new JPanel(new BorderLayout());
		this.add(north, BorderLayout.NORTH);
		
		JPanel south = new JPanel();
		this.add(south, BorderLayout.SOUTH);
		
		JPanel east = new JPanel(new BorderLayout());
		this.add(east, BorderLayout.EAST);
		
		JPanel west = new JPanel(new BorderLayout());
		this.add(west, BorderLayout.WEST);
		
		//text
		
		north.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		//use north panel as margin for inner panel
		
		JPanel northInner = new JPanel(new GridLayout(4, 1, 1, 1));
		northInner.setBorder(BorderFactory.createLineBorder(Color.black));
		northInner.setPreferredSize(new Dimension(-1, 100));
		
		north.add(northInner);
		
		//text
		
		page = new JLabel("Directory: Page 1", SwingConstants.CENTER);
		northInner.add(page);
		
		person = new JLabel("Name: ");
		age = new JLabel("Age: ");
		socialSecurity = new JLabel("Social Security: ");
		
		northInner.add(person);
		northInner.add(age);
		northInner.add(socialSecurity);
		
		//buttons
		
		JButton back = new JButton("<");
		back.setPreferredSize(new Dimension(50, 25));
		south.add(back);
		
		JButton forward = new JButton(">");
		forward.setPreferredSize(new Dimension(50, 25));
		south.add(forward);
		
		//Navigation
		
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				navigate(-1);
			}
			
		});
		
		forward.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				navigate(1);
			}
			
		});
		
		//text fields
		
		JPanel padding = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		padding.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		west.add(padding, BorderLayout.WEST);
		
		JLabel nameSetLabel = new JLabel("Set name");
		JTextField nameSet = new JTextField();
		nameSet.setPreferredSize(new Dimension(300, 20));
		JButton setName = new JButton("Set");
		setName.setPreferredSize(new Dimension(80, 20));
		
		setName.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!nameSet.getText().trim().equals("")) {
					getCurrentPerson().setName(nameSet.getText().trim());
					updatePage();
					nameSet.setText("");
				}
			}
			
		});
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5,  5, 5, 5);
		padding.add(nameSetLabel, gbc);
		gbc.gridx = 1;
		padding.add(nameSet, gbc);
		gbc.gridx = 2;
		padding.add(setName, gbc);
		
		JLabel ageSetLabel = new JLabel("Set Age");
		JTextField ageSet = new JTextField();
		ageSet.setPreferredSize(new Dimension(300, 20));
		JButton setAge = new JButton("Set");
		setAge.setPreferredSize(new Dimension(80, 20));
		
		setAge.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!ageSet.getText().trim().equals("")) {
					try {
						int newAge = Integer.parseInt(ageSet.getText());
						getCurrentPerson().setAge(newAge);
						updatePage();
						ageSet.setText("");
					} catch (Exception ex) {
						//do nothing :)
					}
				}
			}
			
		});
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		padding.add(ageSetLabel, gbc);
		gbc.gridx = 1;
		padding.add(ageSet, gbc);
		gbc.gridx = 2;
		padding.add(setAge, gbc);
		
		JButton youngest = new JButton("Find Youngest");
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		
		padding.add(youngest, gbc);
		
		youngest.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				file.sort();
				currentPage = 1;
				updatePage();
			}
			
		});
		
		JButton add = new JButton("Add Person");
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		
		padding.add(add, gbc);
		
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!nameSet.getText().equals("") && !ageSet.getText().equals("")) {
					try {
						String name = nameSet.getText().trim();
						int age = Integer.parseInt(ageSet.getText());
						Person newPerson = new Person(name, age);
						file.addPerson(newPerson);
						pages++;
						currentPage = pages;
						updatePage();
						nameSet.setText("");
						ageSet.setText("");
					} catch (Exception ex) {
						//do nothing :)
					}
				}
			}
			
		});
		
		JButton delete = new JButton("Delete Person");
		
		gbc.gridx = 2;
		gbc.gridy = 2;
		
		padding.add(delete, gbc);
		
		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (pages != 1) {
					file.getPeople().remove(currentPage - 1);
					if (pages == currentPage)
						currentPage--;
					pages--;
					updatePage();
				}
			}
			
		});
		
	}
	
	private void loadPeople() {
		file = new PersonFile("directory.txt");
		if (!file.getPeople().isEmpty()) {
			currentPage = 1;
			updatePage();
			pages = file.getPeople().size();
		} else {
			System.out.println("Your person directory is empty!");
		}
	}
	
	public void setCurrentPerson(Person person) {
		this.page.setText("Directory: Page " + currentPage);
		this.person.setText("Name: " + person.getName());
		this.age.setText("Age: " + person.getAge());
		this.socialSecurity.setText("Social Security: " + person.getSocialSecurity());
	}
	
	public void updatePage() {
		setCurrentPerson(file.getPeople().get(currentPage - 1));
	}
	
	public Person getCurrentPerson() {
		return file.getPeople().get(currentPage - 1);
	}
	
	public void navigate(int direction) {
		int pageIndex = currentPage - 1;
		if (pageIndex + direction < pages && pageIndex + direction >= 0) {
			currentPage += direction;
			updatePage();
		}
	}
	
	public PersonFile getFile() {
		return file;
	}
	
}
