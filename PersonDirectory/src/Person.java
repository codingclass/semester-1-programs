import java.util.Random;


public class Person implements Comparable<Person> {
	
	private String name;
	private int age;
	private String socialSecurity;
	private final Random random;
	
	public Person(String name, int age) {
		this.random = new Random();
		this.name = name;
		this.age = age;
		this.socialSecurity = generateSocialSecurity();
	}
	
	public Person(String name, int age, String socialSecurity) {
		this(name, age);
		this.socialSecurity = socialSecurity;
	}
	
	@Override
	public int compareTo(Person person) {
		if (this.age < person.getAge())
			return -1;
		if (this.age > person.getAge())
			return 1;
		return 0;
	}
	
	public String generateSocialSecurity() {
		String socialSecurity = "";
		for (int i = 0; i < 3; i ++) {
			socialSecurity += randomDigit();
		}
		socialSecurity += "-";
		for (int i = 0; i < 2; i ++) {
			socialSecurity += randomDigit();
		}
		socialSecurity += "-";
		for (int i = 0; i < 4; i ++) {
			socialSecurity += randomDigit();
		}
		return socialSecurity;
	}
	
	private int randomDigit() {
		return random.nextInt(10) + 1;
	}
	
	//setters
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public boolean setSocialSecurity(String socialSecurity) {
		if (socialSecurity.matches("/d/d/d-/d/d-/d/d/d/d")) {
			this.socialSecurity = socialSecurity;
			return true;
		}
		return false;
	}
	
	//getters
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public String getSocialSecurity() {
		return socialSecurity;
	}
}
