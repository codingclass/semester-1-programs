import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonFile extends File {
	
	private List<Person> currentPeople = new ArrayList<Person>();
	
	public PersonFile(String pathname) {
		super(pathname);
		updatePeople();
	}
	
	public void updatePeople() {
		ArrayList<Person> people = new ArrayList<Person>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(this));
			String line = "";
			String line2 = "";
			String line3 = "";
			while ((line = reader.readLine()) != null) {
				line2 = reader.readLine();
				line3 = reader.readLine();
				people.add(new Person(line, Integer.parseInt(line2), line3));
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.currentPeople = people;
	}
	
	public void addPerson(Person person) {
		this.currentPeople.add(person);
	}
	
	public List<Person> getPeople() {
		return currentPeople;
	}
	
	public void savePeople() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(this));
			for (Person person : currentPeople) {
				writer.write(person.getName() + "\n");
				writer.write(person.getAge() + "\n");
				writer.write(person.getSocialSecurity() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sort() {
		Collections.sort(getPeople());
	}
	
}
