import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;


public class Tester {
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Directory");
		
		DirectoryPanel directoryPanel = new DirectoryPanel();
		
		frame.getContentPane().add(directoryPanel);
		
		/*for (Person person : file.getPeople()) {
			System.out.println(person.getName());
			System.out.println(person.getSocialSecurity());
			System.out.println(person.getAge());
		}*/
		
		frame.pack();
		
		frame.setSize(590, 350);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				directoryPanel.getFile().savePeople();
				System.exit(0);
			}
			
		});
		
	}
	
}
