import java.util.Scanner;

import javax.swing.JFrame;


public class BaseToExponent {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		JFrame frame = new JFrame("Base to Exponent");
		frame.setSize(500, 400);
		B2EPanel panel = new B2EPanel(frame);
		panel.setSize(frame.getSize());
		panel.setLayout(null);
		frame.getContentPane().add(panel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
		/*
		JFileChooser fileChooser = new JFileChooser();
		int choice = fileChooser.showOpenDialog(frame);

		if (choice != JFileChooser.APPROVE_OPTION) {
			System.out.println("You didn't select a file!");
			return;
		}

		File chosenFile = fileChooser.getSelectedFile();
		
		while (true) {
			System.out.println("Enter a base for a power.");
			double base = scanner.nextDouble();
			System.out.println("Thanks, now enter an exponent for a power");
			double power = scanner.nextDouble();
			System.out.println(solveExponent(base, power));
			System.out.println("Would you like to rerun the code? Type yes to rerun, anything else to stop.");
			scanner.nextLine();
			if (!scanner.nextLine().toLowerCase().contains("yes")) break;
		}
		System.out.println("Thank you for using the program!");
		scanner.close();*/
	}
	
	public static String solveExponent(double base, double power) {
		return base + " to the " + power + " power is " + Math.pow(base, power) + "!";
	}
	
}
