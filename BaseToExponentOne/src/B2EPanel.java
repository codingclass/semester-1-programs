import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class B2EPanel extends JPanel {
	
	private JTextField base;
	private JTextField exponent;
	boolean showAnswer;
	double result;
	
	public B2EPanel(JFrame frame) {
		this.setSize(frame.getSize());
		this.setBackground(Color.white);
		initComponents();
		showAnswer = false;
		repaint();
	}
	
	public void initComponents() {
		JButton compute = new JButton("Compute Power");
		compute.setSize(150, 50);
		compute.setLocation((int) this.getWidth() / 2 - compute.getWidth() / 2, (int) this.getHeight() / 2 + 50);
		base = new JTextField("Enter base");
		base.setSize(100, 25);
		base.setLocation((int) this.getWidth() / 4 - base.getWidth() / 2, 50);
		exponent = new JTextField("Enter exponent");
		exponent.setSize(100, 25);
		exponent.setLocation((int) (this.getWidth() / 4 * 3) - base.getWidth() / 2, 50);
		base.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				if (base.getText().equalsIgnoreCase("Enter base"))
					base.setText("");
			}
			
		});
		exponent.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				if (exponent.getText().equalsIgnoreCase("Enter exponent"))
					exponent.setText("");
			}
			
		});
		compute.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				double b = Double.parseDouble(base.getText());
				double exp = Double.parseDouble(exponent.getText());
				result = Math.pow(b, exp);
				showAnswer = true;
				repaint();
			}
			
		});
		this.add(base);
		this.add(exponent);
		this.add(compute);
		base.setVisible(true);
		exponent.setVisible(true);
		compute.setVisible(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.drawString("Type in a base and an exponent and receive an answer!", this.getWidth() / 5, 20);
		if (showAnswer)
			g.drawString("Answer is: " + result, this.getWidth() / 3, this.getHeight() / 2);
	}
	
}
