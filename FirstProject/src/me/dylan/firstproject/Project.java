package me.dylan.firstproject;


public class Project {
	
	/*
	  Dylan Paikin's Code �
	  Testing Project
	 */
	
	public static void main(String[] args) {
		System.out.println("Dylan's Code � \n");
		System.out.print("Hi \"Dylan\" \n");
		System.out.println("Hi \"Dylan\" \n");
		System.out.print(
				" /===========\\\n" +
				"/-------------\\ \n" +
				"|   O     O   |\n" +
				"|             |\n" +
				"|     LL      |\n" +
				"|             |\n" +
				"| -_________- |\n" +
				"|             |\n" +
				" \\___________/\n");
	}
	
}