import java.util.Scanner;


public class PiApproximation {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Please enter the amount of iterations to achieve an approximation of pi.");
			int iterations = -1;
			while (iterations <= 0) {
				iterations = scanner.nextInt();
				if (iterations <= 0) 
					System.out.println("Invalid number of iterations. Enter another number.");
			}
			double pi = 1;
			double fraction = 0;
			for (int i = 1; i <= iterations; i ++) {
				fraction = (double) 1 / (2 * i + 1);
				
				//pi / 4 = 1 - 1/3 + 1/5 - 1/7 + 1/9...
				if (i % 2 == 0)
					pi += fraction;
				else
					pi -= fraction;
			}
			pi *= 4;
			System.out.println("The approximate number of pi for " + iterations + " iterations is " + pi + "!");
			System.out.println("Would you like to rerun the code? Type yes to rerun, anything else to stop.");
			scanner.nextLine();
			if (!scanner.nextLine().toLowerCase().contains("yes")) break;
		}
		System.out.println("Thank you for using the Pi Approixmator.");
		scanner.close();
	}
	
}
