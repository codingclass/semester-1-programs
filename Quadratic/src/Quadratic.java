import java.util.Scanner;


public class Quadratic {
	
	private static Scanner scanner = null;
	
	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		promptQuadratic();
	}

	public static void promptQuadratic() {
		System.out.println("Please enter the co-efficients for the quadratic formula");
		System.out.println("A:");
		double a = -1;
		while (a <= 0) {
			a = scanner.nextDouble();
			if (a <= 0)
				System.out.println("Invalid A, it must be greater than 0!");
			else
				break;
		}
		System.out.println("B:");
		double b = scanner.nextDouble();
		System.out.println("C:");
		double c = scanner.nextDouble();
		System.out.println("Answer(s): " + solveQuadratic(a, b, c));
	}
	
	public static String solveQuadratic(double a, double b, double c) {
		double discriminant = Math.pow(b, 2) - 4 * a * c;
		if (discriminant < 0)
			return "(" + -b + " + sqrt(" + -discriminant + ")i)/" + 2*a + ", (" + -b + " - sqrt(" + -discriminant + ")i) / " + 2*a;
		else if (discriminant > 0)
			return "(" + (-b) + " + sqrt(" + discriminant + ")) / " + (2 * a) + ", (" + (-b) + " - sqrt(" + discriminant + ")) / " + (2 * a) + ",";
		else
			return (-b) + " / " + (2 * a);
	}
	
}
