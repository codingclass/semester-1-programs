import javax.swing.JButton;
import javax.swing.JFrame;


public class TestScoresView extends JFrame {
	
	private TestScoresModel model;
	
	private JButton addButton = new JButton("Add");
	private JButton modifyButton = new JButton("Modify");
	private JButton firstButton = new JButton("<<");
	private JButton previousButton = new JButton("<");
	private JButton nextButton = new JButton(">");
	private JButton lastButton = new JButton(">>");
	private JButton highScoreButton = new JButton("Highest Score");
	private JButton aveScoreButton = new JButton("Class Average");
	
}
