import java.util.InputMismatchException;
import java.util.Scanner;


public class NChooseK {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Please enter n, or enter -1 to quit!");
			int n = 0;
			try {
				n = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Not a valid integer!");
				scanner.nextLine();
			}
			if (n == -1) {
				break;
			}
			int k = 0;
			System.out.println("Please enter k, or enter -1 to quit!");
			try {
				k = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Not a valid integer!");
				scanner.nextLine();
			}
			if (n == -1) {
				break;
			}
			System.out.println("Your value is " + nChooseK(n, k));
		}
		scanner.close();
		System.out.println("Thank you for using nChooseK!");
	}
	
	private static long nChooseK(int n, int k) {
		return factorial(n) / (factorial(k) * factorial(n - k));
	}
	
	private static long factorial(int factorial) {
		return factorial == 0 ? 1 : factorial * factorial(factorial - 1);
	}
	
}
