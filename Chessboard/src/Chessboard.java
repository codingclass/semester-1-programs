import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Chessboard {
	
	public static void main(String[] args) {
		JFrame board = new JFrame("Chessboard");
		board.setResizable(true);
		board.setSize(500, 500);
		board.setLocationRelativeTo(null);
		
		JPanel boardPanel = new JPanel();
		
		boardPanel.setLayout(new GridLayout(8, 8));
		for (int x = 0; x < 8; x ++) {
			for (int y = 0; y < 8; y ++) {
				JPanel panel = null;
				if ((x + y) % 2 == 0)
					panel = new ColorPanel(Color.white);
				else
					panel = new ColorPanel(Color.black);
				boardPanel.add(panel);
			}
		}
		
		board.add(boardPanel);
		board.setVisible(true);
		
	}
	
	public static class ColorPanel extends JPanel {
		
		public ColorPanel(Color color) {
			setBackground(color);
		}
		
	}
	
}
