package me.dylan.brainfuck;

import java.util.Scanner;

public class Brainfuck {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter the word to convert to Brainfuck:");
		String word = scanner.nextLine();
		for (int i = 0; i < word.length(); i++){
		    char c = word.charAt(i);        
		    System.out.println(makeCharacter(c));
		}
		System.out.println("> + [ > ++++++++++ < - ] > .");
		scanner.close();
	}
	
	public static String makeCharacter(char c) {
		int num = (int) c;
		int tens = (int) Math.floor(num / 10);
		String line = "> ";
		for (int i = 0; i < tens; i ++) {
			line += "+";
		}
		line += " [ > ++++++++++ < - ] > ";
		int remainder = num % 10;
		for (int i = 0; i < remainder; i ++) {
			line += "+";
		}
		line += " .";
		return line;
	}
	
}
