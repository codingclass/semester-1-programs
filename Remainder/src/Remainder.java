import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;


public class Remainder {
	
	public static void main(String[] args) {
		System.out.println("Welcome to 123-ABC Math, here we teach our children the foundations of basic math principals.\nWith our expertise your child will be fostered with the greatest math knowledge when he is ready for college.\n");
		System.out.println("Please select the text file.");
		JFrame jframe = new JFrame("File Browser");
		JFileChooser chooser= new JFileChooser();
		jframe.add(chooser);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.addChoosableFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return null;
			}
			
			@Override
			public boolean accept(File f) {
				if (f.getName().endsWith(".txt") || !f.getName().contains("."))
					return true;
				return false;
			}
			
		});
		int choice = chooser.showOpenDialog(jframe);

		if (choice != JFileChooser.APPROVE_OPTION) {
			System.out.println("You didn't select a file!");
			return;
		}

		File chosenFile = chooser.getSelectedFile();
		System.out.println(chosenFile.getName() + " was chosen");
		File output = new File(chosenFile.getParentFile().getPath() + "/output.txt");
		FileOutputStream fos = null;
		if (!output.exists()) {
			try {
				output.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			fos = new FileOutputStream(output);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
		try {
			BufferedReader reader = new BufferedReader(new FileReader(chosenFile));
			String line = null;
			while((line = reader.readLine()) != null ) {
				String[] lines = line.split("\\s+");
				if (lines.length == 2) {
					if(line.matches(".*\\d.*")){
						double num1 = Double.parseDouble(lines[0]);
						double num2 = Double.parseDouble(lines[1]);
						if (num1 !=0 && num2 !=0) {
							Division division = new Division(num1, num2);
							write(writer, division.getLarge() + " / " + division.getSmall() + " = " + division.getAnswer());
						} else
							write(writer, num1 + " and " + num2 + " cause a divide by zero error.");
					} else
						write(writer, "Line contains invalid characters");
				}
			}
			reader.close();
			writer.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (Desktop.isDesktopSupported()) {
		    try {
				Desktop.getDesktop().edit(output);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Congratulations your child is now ready for college.");
		System.out.println("Please read output.txt in the folder of the number file for solutions!");
	}
	
	public static void write(BufferedWriter writer, String line) {
		try {
			writer.write(line);
			for (int i = 0; i < 2; i ++)
				writer.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}