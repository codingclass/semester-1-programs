
public class Division {
	
	private final int quotient;
	private final double remainder;
	private final double large;
	private final double small;
	
	public Division(double num1, double num2) {
		if (num1 > num2) {
			quotient = (int) (num1 / num2);
			remainder = num1 % num2;
			large = num1;
			small = num2;
		} else {
			quotient = (int) (num2 / num1);
			remainder = num2 % num1;
			large = num2;
			small = num1;
		}
	}
	
	public int getQuotient() {
		return quotient;
	}
	
	public double getRemainder() {
		return remainder;
	}
	
	public String getString() {
		return "The quotient is " + quotient + " and the remainder is " + remainder + "!";
	}
	
	public String getAnswer() {
		return remainder == 0 ? "" + quotient : quotient + "R" + remainder;
	}
	
	public double getLarge() {
		return large;
	}
	
	public double getSmall() {
		return small;
	}
	
}
