package me.dylan.hangman;

import java.awt.Dimension;

import javax.swing.JFrame;

import me.dylan.hangman.game.HangmanGame;

public class Hangman {
	
	public final static Dimension RESOLUTION = new Dimension(1280, 960);
	
	public static void main(String[] args) {
		JFrame hangmanFrame = new JFrame("Hangman");
		hangmanFrame.setSize(RESOLUTION);
		hangmanFrame.setLocationRelativeTo(null);
		hangmanFrame.setVisible(true);
		hangmanFrame.getContentPane().add(new HangmanGame());
	}
	
}
