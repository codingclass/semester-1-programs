import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Illusion {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Illusion");
		frame.setLayout(new GridLayout(3, 6));
		ColorPanel panel = new ColorPanel(Color.black);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 6; y ++) {
				if (x >= 3 && x < 6) {
					panel = new ColorPanel(Color.white);
				} else if (x + y == 7 || x + y == 10) {
					panel = new ColorPanel(Color.lightGray);
				} else if (x + y == 9 || x + y == 11) {
					panel = new ColorPanel(Color.white);
				} else if (x + y > 15) {
					panel = new ColorPanel(Color.white);
				} else {
					panel = new ColorPanel(Color.black);
				}
				frame.add(panel);
			}
		}
		frame.setSize(600, 600);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private static class ColorPanel extends JPanel {
		
		public ColorPanel(Color color) {
			setBackground(color);
		}
		
	}
	
}


