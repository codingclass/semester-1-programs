
public interface Account {
	
	public void deposit(double money);
	public boolean withdrawal(double money);
	public double getBalance();
	
}
