
public class SavingsAccount extends CheckingAccount {
	
	private double interest;
	
	public SavingsAccount(double bal, double interestRate) {
		super(bal);
		this.interest = interestRate;
	}
	
	public SavingsAccount(double interestRate) {
		this(0.0D, interestRate);
	}
	
	@Override
	public void deposit(double money) {
		super.deposit(money);
		super.money *= (1 + interest);
	}
	
}
