import java.util.InputMismatchException;
import java.util.Scanner;


public class Bank {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Would you like to open a checking (1), savings (2), or credit (3) account?");
		
		int selection;
		
		while (true) {
			
			try {
				selection = scanner.nextInt();
				if (selection == 1 || selection == 2 || selection == 3) {
					break;
				}
				System.out.println("That is not a valid choice!");
			} catch (InputMismatchException e) {
				System.out.println("That is not a valid choice!");
				scanner.nextLine();
			}
			
		}
		
		if (selection == 1) { //checking
			System.out.println("How much money would you like to have in your account?");
			double money = 0;
			
			while(true) {
				
				try {
					money = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid money amount!");
					scanner.nextLine();
				}
				
			}
			
			AbstractAccount checking = new CheckingAccount(money);
			
			while (true) {
				
				System.out.println("Your current checking account balance is $" + checking.getBalance());
				System.out.println("Would you like to deposit(1) or withdrawl(2)? To quit type (3).");
				
				while (true) {
					
					try {
						selection = scanner.nextInt();
						if (selection == 1 || selection == 2 || selection == 3) {
							break;
						}
						System.out.println("That is not a valid choice!");
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid choice!");
						scanner.nextLine();
					}
					
				}
				
				if (selection == 1) {
					System.out.println("How much money would you like to deposit?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					checking.deposit(money);
					System.out.println("You have deposited $" + money + "!");
				} else if (selection == 2) {
					System.out.println("How much money would you like to withdrawal?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					if (checking.withdrawal(money))
						System.out.println("You have widthdrawn $" + money + "!");
					else
						System.out.println("You do not have enough money to perform this action!");
				} else if (selection == 3) {
					System.out.println("Thank you for using the checking account. Your final balance was $" + checking.getBalance() + "!");
					break;
				}
				
			}
			
		} else if (selection == 2) { //savings
			System.out.println("How much money would you like to have in your account?");
			double money = 0;
			
			while(true) {
				
				try {
					money = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid money amount!");
					scanner.nextLine();
				}
				
			}
			
			System.out.println("What should the interest rate be for your account?");
			double rate = 0;
			
			while(true) {
				
				try {
					rate = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid money amount!");
					scanner.nextLine();
				}
				
			}
			
			AbstractAccount savings = new SavingsAccount(money, rate);
			
			while (true) {
				
				System.out.println("Your current savings account balance is $" + savings.getBalance());
				System.out.println("Would you like to deposit(1) or withdrawl(2)? To quit type (3).");
				
				while (true) {
					
					try {
						selection = scanner.nextInt();
						if (selection == 1 || selection == 2 || selection == 3) {
							break;
						}
						System.out.println("That is not a valid choice!");
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid choice!");
						scanner.nextLine();
					}
					
				}
				
				if (selection == 1) {
					System.out.println("How much money would you like to deposit?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					savings.deposit(money);
					System.out.println("You have deposited $" + money + "!");
				} else if (selection == 2) {
					System.out.println("How much money would you like to withdrawal?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					if (savings.withdrawal(money))
						System.out.println("You have widthdrawn $" + money + "!");
					else
						System.out.println("You do not have enough money to perform this action!");
				} else if (selection == 3) {
					System.out.println("Thank you for using the savings account. Your final balance was $" + savings.getBalance() + "!");
					break;
				}
				
			}
			
		} else if (selection == 3) { //credit
			
			System.out.println("How much money would you like the max of your account to be?");
			double max = 0;
			
			while(true) {
				
				try {
					max = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid money amount!");
					scanner.nextLine();
				}
				
			}
			
			double money = 0;
			
			System.out.println("What should the interest rate be for your account?");
			double rate = 0;
			
			while(true) {
				
				try {
					rate = scanner.nextDouble();
					break;
				} catch (InputMismatchException e) {
					System.out.println("That is not a valid money amount!");
					scanner.nextLine();
				}
				
			}
			
			AbstractAccount credit = new CreditAccount(max, rate);
			
			while (true) {
				
				System.out.println("Your current credit account balance is $" + credit.getBalance() + " out of $" + ((CreditAccount) credit).getMax() + "!");
				System.out.println("Would you like to deposit(1) or withdrawl(2)? To quit type (3).");
				
				while (true) {
					
					try {
						selection = scanner.nextInt();
						if (selection == 1 || selection == 2 || selection == 3) {
							break;
						}
						System.out.println("That is not a valid choice!");
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid choice!");
						scanner.nextLine();
					}
					
				}
				
				if (selection == 1) {
					System.out.println("How much money would you like to deposit?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					credit.deposit(money);
					System.out.println("You have deposited $" + money + "!");
				} else if (selection == 2) {
					System.out.println("How much money would you like to withdrawal?");
					
					while(true) {
						
						try {
							money = scanner.nextDouble();
							break;
						} catch (InputMismatchException e) {
							System.out.println("That is not a valid money amount!");
							scanner.nextLine();
						}
						
					}
					
					if (credit.withdrawal(money))
						System.out.println("You have widthdrawn $" + money + "!");
					else
						System.out.println("You do not have enough credit to perform this action!");
				} else if (selection == 3) {
					System.out.println("Thank you for using the savings account. Your final balance was $" + credit.getBalance() + " out of " + ((CreditAccount) credit).getMax() + "!");
					break;
				}
				
			}
			
		}
		
	}
	
}
