
public abstract class AbstractAccount implements Account {
	
	protected double money;
	
	protected AbstractAccount(double money) {
		this.money = money;
	}
	
	@Override
	public double getBalance() { 
		return money;
	}
	
}
