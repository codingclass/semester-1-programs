
public class CreditAccount extends AbstractAccount {
	
	private double interest;
	private double max;
	
	public CreditAccount(double max, double interest, double bal) {
		super(bal);
		this.interest = interest;
		this.max = max;
	}
	
	public CreditAccount(double max, double interest) {
		this(max, interest, 0.0D);
	}
	
	public double getMax() {
		return max;
	}
	
	@Override
	public void deposit(double money) {
		if (super.money - money < 0)
			super.money = 0;
		else
			super.money -= money;
	}

	@Override
	public boolean withdrawal(double money) {
		if (max - super.money >= money) {
			super.money += money;
			super.money *= interest + 1;
			return true;
		}
		return false;
	}
	
}
