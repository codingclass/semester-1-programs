
public class CheckingAccount extends AbstractAccount {
	
	public CheckingAccount(double bal) {
		super(bal);
	}

	@Override
	public void deposit(double money) {
		super.money += money;
	}

	@Override
	public boolean withdrawal(double money) {
		if (super.money >= money) {
			super.money -= money;
			return true;
		}
		return false;
	}
	
}
